package com.haui.adidasshoes.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Size.
 */
@Entity
@Table(name = "size")
public class Size implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private Float name;

    @Column(name = "decription")
    private String decription;

    @OneToMany(mappedBy = "size")
    private Set<ProductAmount> productAmounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getName() {
        return name;
    }

    public Size name(Float name) {
        this.name = name;
        return this;
    }

    public void setName(Float name) {
        this.name = name;
    }

    public String getDecription() {
        return decription;
    }

    public Size decription(String decription) {
        this.decription = decription;
        return this;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public Set<ProductAmount> getProductAmounts() {
        return productAmounts;
    }

    public Size productAmounts(Set<ProductAmount> productAmounts) {
        this.productAmounts = productAmounts;
        return this;
    }

    public Size addProductAmount(ProductAmount productAmount) {
        this.productAmounts.add(productAmount);
        productAmount.setSize(this);
        return this;
    }

    public Size removeProductAmount(ProductAmount productAmount) {
        this.productAmounts.remove(productAmount);
        productAmount.setSize(null);
        return this;
    }

    public void setProductAmounts(Set<ProductAmount> productAmounts) {
        this.productAmounts = productAmounts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Size)) {
            return false;
        }
        return id != null && id.equals(((Size) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Size{" +
            "id=" + getId() +
            ", name=" + getName() +
            ", decription='" + getDecription() + "'" +
            "}";
    }
}
