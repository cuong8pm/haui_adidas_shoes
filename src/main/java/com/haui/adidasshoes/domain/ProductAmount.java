package com.haui.adidasshoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProductAmount.
 */
@Entity
@Table(name = "product_amount")
public class ProductAmount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total")
    private Double total;

    @Column(name = "inventory")
    private Double inventory;

    @Column(name = "product_sold")
    private Double productSold;

    @ManyToOne
    @JsonIgnoreProperties(value = "productAmounts", allowSetters = true)
    private Product product;

    @ManyToOne
    @JsonIgnoreProperties(value = "productAmounts", allowSetters = true)
    private Size size;

    @OneToMany(mappedBy = "productAmount")
    private Set<ProductImportHistory> productImportHistories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotal() {
        return total;
    }

    public ProductAmount total(Double total) {
        this.total = total;
        return this;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getInventory() {
        return inventory;
    }

    public ProductAmount inventory(Double inventory) {
        this.inventory = inventory;
        return this;
    }

    public void setInventory(Double inventory) {
        this.inventory = inventory;
    }

    public Double getProductSold() {
        return productSold;
    }

    public ProductAmount productSold(Double productSold) {
        this.productSold = productSold;
        return this;
    }

    public void setProductSold(Double productSold) {
        this.productSold = productSold;
    }

    public Product getProduct() {
        return product;
    }

    public ProductAmount product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Size getSize() {
        return size;
    }

    public ProductAmount size(Size size) {
        this.size = size;
        return this;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Set<ProductImportHistory> getProductImportHistories() {
        return productImportHistories;
    }

    public ProductAmount productImportHistories(Set<ProductImportHistory> productImportHistories) {
        this.productImportHistories = productImportHistories;
        return this;
    }

    public ProductAmount addProductImportHistory(ProductImportHistory productImportHistory) {
        this.productImportHistories.add(productImportHistory);
        productImportHistory.setProductAmount(this);
        return this;
    }

    public ProductAmount removeProductImportHistory(ProductImportHistory productImportHistory) {
        this.productImportHistories.remove(productImportHistory);
        productImportHistory.setProductAmount(null);
        return this;
    }

    public void setProductImportHistories(Set<ProductImportHistory> productImportHistories) {
        this.productImportHistories = productImportHistories;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductAmount)) {
            return false;
        }
        return id != null && id.equals(((ProductAmount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductAmount{" +
            "id=" + getId() +
            ", total=" + getTotal() +
            ", inventory=" + getInventory() +
            ", productSold=" + getProductSold() +
            "}";
    }
}
