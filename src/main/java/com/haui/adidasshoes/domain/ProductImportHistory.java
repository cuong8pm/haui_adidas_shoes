package com.haui.adidasshoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ProductImportHistory.
 */
@Entity
@Table(name = "product_import_history")
public class ProductImportHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "log")
    private String log;

    @ManyToOne
    @JsonIgnoreProperties(value = "productImportHistories", allowSetters = true)
    private ProductAmount productAmount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLog() {
        return log;
    }

    public ProductImportHistory log(String log) {
        this.log = log;
        return this;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public ProductAmount getProductAmount() {
        return productAmount;
    }

    public ProductImportHistory productAmount(ProductAmount productAmount) {
        this.productAmount = productAmount;
        return this;
    }

    public void setProductAmount(ProductAmount productAmount) {
        this.productAmount = productAmount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductImportHistory)) {
            return false;
        }
        return id != null && id.equals(((ProductImportHistory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductImportHistory{" +
            "id=" + getId() +
            ", log='" + getLog() + "'" +
            "}";
    }
}
