package com.haui.adidasshoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @Column(name = "price")
    private Double price;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "discount")
    private Integer discount;

    @ManyToOne
    @JsonIgnoreProperties(value = "products", allowSetters = true)
    private Categori categori;

    @OneToMany(mappedBy = "product")
    private Set<ProductDetails> productDetails = new HashSet<>();

    @OneToMany(mappedBy = "product")
    private Set<ProductImage> productImages = new HashSet<>();

    @OneToMany(mappedBy = "product")
    private Set<ProductAmount> productAmounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public Product photo(byte[] photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public Product photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public Double getPrice() {
        return price;
    }

    public Product price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public Product title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Product description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDiscount() {
        return discount;
    }

    public Product discount(Integer discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Categori getCategori() {
        return categori;
    }

    public Product categori(Categori categori) {
        this.categori = categori;
        return this;
    }

    public void setCategori(Categori categori) {
        this.categori = categori;
    }

    public Set<ProductDetails> getProductDetails() {
        return productDetails;
    }

    public Product productDetails(Set<ProductDetails> productDetails) {
        this.productDetails = productDetails;
        return this;
    }

    public Product addProductDetails(ProductDetails productDetails) {
        this.productDetails.add(productDetails);
        productDetails.setProduct(this);
        return this;
    }

    public Product removeProductDetails(ProductDetails productDetails) {
        this.productDetails.remove(productDetails);
        productDetails.setProduct(null);
        return this;
    }

    public void setProductDetails(Set<ProductDetails> productDetails) {
        this.productDetails = productDetails;
    }

    public Set<ProductImage> getProductImages() {
        return productImages;
    }

    public Product productImages(Set<ProductImage> productImages) {
        this.productImages = productImages;
        return this;
    }

    public Product addProductImage(ProductImage productImage) {
        this.productImages.add(productImage);
        productImage.setProduct(this);
        return this;
    }

    public Product removeProductImage(ProductImage productImage) {
        this.productImages.remove(productImage);
        productImage.setProduct(null);
        return this;
    }

    public void setProductImages(Set<ProductImage> productImages) {
        this.productImages = productImages;
    }

    public Set<ProductAmount> getProductAmounts() {
        return productAmounts;
    }

    public Product productAmounts(Set<ProductAmount> productAmounts) {
        this.productAmounts = productAmounts;
        return this;
    }

    public Product addProductAmount(ProductAmount productAmount) {
        this.productAmounts.add(productAmount);
        productAmount.setProduct(this);
        return this;
    }

    public Product removeProductAmount(ProductAmount productAmount) {
        this.productAmounts.remove(productAmount);
        productAmount.setProduct(null);
        return this;
    }

    public void setProductAmounts(Set<ProductAmount> productAmounts) {
        this.productAmounts = productAmounts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return id != null && id.equals(((Product) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + getPhotoContentType() + "'" +
            ", price=" + getPrice() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", discount=" + getDiscount() +
            "}";
    }
}
