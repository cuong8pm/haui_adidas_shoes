package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.Categori;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Categori entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoriRepository extends JpaRepository<Categori, Long> {
}
