package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.ProductAmount;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProductAmount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductAmountRepository extends JpaRepository<ProductAmount, Long> {
}
