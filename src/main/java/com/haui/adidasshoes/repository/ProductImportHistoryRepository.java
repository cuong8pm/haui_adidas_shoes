package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.ProductImportHistory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProductImportHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductImportHistoryRepository extends JpaRepository<ProductImportHistory, Long> {
}
