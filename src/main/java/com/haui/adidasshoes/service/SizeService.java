package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.Size;
import com.haui.adidasshoes.repository.SizeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Size}.
 */
@Service
@Transactional
public class SizeService {

    private final Logger log = LoggerFactory.getLogger(SizeService.class);

    private final SizeRepository sizeRepository;

    public SizeService(SizeRepository sizeRepository) {
        this.sizeRepository = sizeRepository;
    }

    /**
     * Save a size.
     *
     * @param size the entity to save.
     * @return the persisted entity.
     */
    public Size save(Size size) {
        log.debug("Request to save Size : {}", size);
        return sizeRepository.save(size);
    }

    /**
     * Get all the sizes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Size> findAll(Pageable pageable) {
        log.debug("Request to get all Sizes");
        return sizeRepository.findAll(pageable);
    }


    /**
     * Get one size by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Size> findOne(Long id) {
        log.debug("Request to get Size : {}", id);
        return sizeRepository.findById(id);
    }

    /**
     * Delete the size by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Size : {}", id);
        sizeRepository.deleteById(id);
    }
}
