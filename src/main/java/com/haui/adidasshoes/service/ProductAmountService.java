package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.ProductAmount;
import com.haui.adidasshoes.repository.ProductAmountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductAmount}.
 */
@Service
@Transactional
public class ProductAmountService {

    private final Logger log = LoggerFactory.getLogger(ProductAmountService.class);

    private final ProductAmountRepository productAmountRepository;

    public ProductAmountService(ProductAmountRepository productAmountRepository) {
        this.productAmountRepository = productAmountRepository;
    }

    /**
     * Save a productAmount.
     *
     * @param productAmount the entity to save.
     * @return the persisted entity.
     */
    public ProductAmount save(ProductAmount productAmount) {
        log.debug("Request to save ProductAmount : {}", productAmount);
        return productAmountRepository.save(productAmount);
    }

    /**
     * Get all the productAmounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductAmount> findAll(Pageable pageable) {
        log.debug("Request to get all ProductAmounts");
        return productAmountRepository.findAll(pageable);
    }


    /**
     * Get one productAmount by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProductAmount> findOne(Long id) {
        log.debug("Request to get ProductAmount : {}", id);
        return productAmountRepository.findById(id);
    }

    /**
     * Delete the productAmount by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductAmount : {}", id);
        productAmountRepository.deleteById(id);
    }
}
