package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.Categori;
import com.haui.adidasshoes.repository.CategoriRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Categori}.
 */
@Service
@Transactional
public class CategoriService {

    private final Logger log = LoggerFactory.getLogger(CategoriService.class);

    private final CategoriRepository categoriRepository;

    public CategoriService(CategoriRepository categoriRepository) {
        this.categoriRepository = categoriRepository;
    }

    /**
     * Save a categori.
     *
     * @param categori the entity to save.
     * @return the persisted entity.
     */
    public Categori save(Categori categori) {
        log.debug("Request to save Categori : {}", categori);
        return categoriRepository.save(categori);
    }

    /**
     * Get all the categoris.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Categori> findAll(Pageable pageable) {
        log.debug("Request to get all Categoris");
        return categoriRepository.findAll(pageable);
    }


    /**
     * Get one categori by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Categori> findOne(Long id) {
        log.debug("Request to get Categori : {}", id);
        return categoriRepository.findById(id);
    }

    /**
     * Delete the categori by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Categori : {}", id);
        categoriRepository.deleteById(id);
    }
}
