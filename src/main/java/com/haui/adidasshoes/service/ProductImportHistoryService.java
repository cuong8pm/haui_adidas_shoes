package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.ProductImportHistory;
import com.haui.adidasshoes.repository.ProductImportHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductImportHistory}.
 */
@Service
@Transactional
public class ProductImportHistoryService {

    private final Logger log = LoggerFactory.getLogger(ProductImportHistoryService.class);

    private final ProductImportHistoryRepository productImportHistoryRepository;

    public ProductImportHistoryService(ProductImportHistoryRepository productImportHistoryRepository) {
        this.productImportHistoryRepository = productImportHistoryRepository;
    }

    /**
     * Save a productImportHistory.
     *
     * @param productImportHistory the entity to save.
     * @return the persisted entity.
     */
    public ProductImportHistory save(ProductImportHistory productImportHistory) {
        log.debug("Request to save ProductImportHistory : {}", productImportHistory);
        return productImportHistoryRepository.save(productImportHistory);
    }

    /**
     * Get all the productImportHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductImportHistory> findAll(Pageable pageable) {
        log.debug("Request to get all ProductImportHistories");
        return productImportHistoryRepository.findAll(pageable);
    }


    /**
     * Get one productImportHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProductImportHistory> findOne(Long id) {
        log.debug("Request to get ProductImportHistory : {}", id);
        return productImportHistoryRepository.findById(id);
    }

    /**
     * Delete the productImportHistory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductImportHistory : {}", id);
        productImportHistoryRepository.deleteById(id);
    }
}
