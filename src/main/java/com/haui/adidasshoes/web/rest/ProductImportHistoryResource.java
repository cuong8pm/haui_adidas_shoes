package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.ProductImportHistory;
import com.haui.adidasshoes.service.ProductImportHistoryService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.ProductImportHistory}.
 */
@RestController
@RequestMapping("/api")
public class ProductImportHistoryResource {

    private final Logger log = LoggerFactory.getLogger(ProductImportHistoryResource.class);

    private static final String ENTITY_NAME = "productImportHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductImportHistoryService productImportHistoryService;

    public ProductImportHistoryResource(ProductImportHistoryService productImportHistoryService) {
        this.productImportHistoryService = productImportHistoryService;
    }

    /**
     * {@code POST  /product-import-histories} : Create a new productImportHistory.
     *
     * @param productImportHistory the productImportHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productImportHistory, or with status {@code 400 (Bad Request)} if the productImportHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-import-histories")
    public ResponseEntity<ProductImportHistory> createProductImportHistory(@RequestBody ProductImportHistory productImportHistory) throws URISyntaxException {
        log.debug("REST request to save ProductImportHistory : {}", productImportHistory);
        if (productImportHistory.getId() != null) {
            throw new BadRequestAlertException("A new productImportHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductImportHistory result = productImportHistoryService.save(productImportHistory);
        return ResponseEntity.created(new URI("/api/product-import-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-import-histories} : Updates an existing productImportHistory.
     *
     * @param productImportHistory the productImportHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productImportHistory,
     * or with status {@code 400 (Bad Request)} if the productImportHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productImportHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-import-histories")
    public ResponseEntity<ProductImportHistory> updateProductImportHistory(@RequestBody ProductImportHistory productImportHistory) throws URISyntaxException {
        log.debug("REST request to update ProductImportHistory : {}", productImportHistory);
        if (productImportHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductImportHistory result = productImportHistoryService.save(productImportHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productImportHistory.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-import-histories} : get all the productImportHistories.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productImportHistories in body.
     */
    @GetMapping("/product-import-histories")
    public ResponseEntity<List<ProductImportHistory>> getAllProductImportHistories(Pageable pageable) {
        log.debug("REST request to get a page of ProductImportHistories");
        Page<ProductImportHistory> page = productImportHistoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /product-import-histories/:id} : get the "id" productImportHistory.
     *
     * @param id the id of the productImportHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productImportHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-import-histories/{id}")
    public ResponseEntity<ProductImportHistory> getProductImportHistory(@PathVariable Long id) {
        log.debug("REST request to get ProductImportHistory : {}", id);
        Optional<ProductImportHistory> productImportHistory = productImportHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productImportHistory);
    }

    /**
     * {@code DELETE  /product-import-histories/:id} : delete the "id" productImportHistory.
     *
     * @param id the id of the productImportHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-import-histories/{id}")
    public ResponseEntity<Void> deleteProductImportHistory(@PathVariable Long id) {
        log.debug("REST request to delete ProductImportHistory : {}", id);
        productImportHistoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
