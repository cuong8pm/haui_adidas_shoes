package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.Categori;
import com.haui.adidasshoes.service.CategoriService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.Categori}.
 */
@RestController
@RequestMapping("/api")
public class CategoriResource {

    private final Logger log = LoggerFactory.getLogger(CategoriResource.class);

    private static final String ENTITY_NAME = "categori";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategoriService categoriService;

    public CategoriResource(CategoriService categoriService) {
        this.categoriService = categoriService;
    }

    /**
     * {@code POST  /categoris} : Create a new categori.
     *
     * @param categori the categori to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categori, or with status {@code 400 (Bad Request)} if the categori has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/categoris")
    public ResponseEntity<Categori> createCategori(@RequestBody Categori categori) throws URISyntaxException {
        log.debug("REST request to save Categori : {}", categori);
        if (categori.getId() != null) {
            throw new BadRequestAlertException("A new categori cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Categori result = categoriService.save(categori);
        return ResponseEntity.created(new URI("/api/categoris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /categoris} : Updates an existing categori.
     *
     * @param categori the categori to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categori,
     * or with status {@code 400 (Bad Request)} if the categori is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categori couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/categoris")
    public ResponseEntity<Categori> updateCategori(@RequestBody Categori categori) throws URISyntaxException {
        log.debug("REST request to update Categori : {}", categori);
        if (categori.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Categori result = categoriService.save(categori);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, categori.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /categoris} : get all the categoris.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categoris in body.
     */
    @GetMapping("/categoris")
    public ResponseEntity<List<Categori>> getAllCategoris(Pageable pageable) {
        log.debug("REST request to get a page of Categoris");
        Page<Categori> page = categoriService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /categoris/:id} : get the "id" categori.
     *
     * @param id the id of the categori to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categori, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/categoris/{id}")
    public ResponseEntity<Categori> getCategori(@PathVariable Long id) {
        log.debug("REST request to get Categori : {}", id);
        Optional<Categori> categori = categoriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(categori);
    }

    /**
     * {@code DELETE  /categoris/:id} : delete the "id" categori.
     *
     * @param id the id of the categori to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categoris/{id}")
    public ResponseEntity<Void> deleteCategori(@PathVariable Long id) {
        log.debug("REST request to delete Categori : {}", id);
        categoriService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
