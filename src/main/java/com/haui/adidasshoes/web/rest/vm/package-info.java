/**
 * View Models used by Spring MVC REST controllers.
 */
package com.haui.adidasshoes.web.rest.vm;
