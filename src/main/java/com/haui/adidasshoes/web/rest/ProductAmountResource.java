package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.ProductAmount;
import com.haui.adidasshoes.service.ProductAmountService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.ProductAmount}.
 */
@RestController
@RequestMapping("/api")
public class ProductAmountResource {

    private final Logger log = LoggerFactory.getLogger(ProductAmountResource.class);

    private static final String ENTITY_NAME = "productAmount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductAmountService productAmountService;

    public ProductAmountResource(ProductAmountService productAmountService) {
        this.productAmountService = productAmountService;
    }

    /**
     * {@code POST  /product-amounts} : Create a new productAmount.
     *
     * @param productAmount the productAmount to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productAmount, or with status {@code 400 (Bad Request)} if the productAmount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-amounts")
    public ResponseEntity<ProductAmount> createProductAmount(@RequestBody ProductAmount productAmount) throws URISyntaxException {
        log.debug("REST request to save ProductAmount : {}", productAmount);
        if (productAmount.getId() != null) {
            throw new BadRequestAlertException("A new productAmount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductAmount result = productAmountService.save(productAmount);
        return ResponseEntity.created(new URI("/api/product-amounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-amounts} : Updates an existing productAmount.
     *
     * @param productAmount the productAmount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productAmount,
     * or with status {@code 400 (Bad Request)} if the productAmount is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productAmount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-amounts")
    public ResponseEntity<ProductAmount> updateProductAmount(@RequestBody ProductAmount productAmount) throws URISyntaxException {
        log.debug("REST request to update ProductAmount : {}", productAmount);
        if (productAmount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductAmount result = productAmountService.save(productAmount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productAmount.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-amounts} : get all the productAmounts.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productAmounts in body.
     */
    @GetMapping("/product-amounts")
    public ResponseEntity<List<ProductAmount>> getAllProductAmounts(Pageable pageable) {
        log.debug("REST request to get a page of ProductAmounts");
        Page<ProductAmount> page = productAmountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /product-amounts/:id} : get the "id" productAmount.
     *
     * @param id the id of the productAmount to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productAmount, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-amounts/{id}")
    public ResponseEntity<ProductAmount> getProductAmount(@PathVariable Long id) {
        log.debug("REST request to get ProductAmount : {}", id);
        Optional<ProductAmount> productAmount = productAmountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productAmount);
    }

    /**
     * {@code DELETE  /product-amounts/:id} : delete the "id" productAmount.
     *
     * @param id the id of the productAmount to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-amounts/{id}")
    public ResponseEntity<Void> deleteProductAmount(@PathVariable Long id) {
        log.debug("REST request to delete ProductAmount : {}", id);
        productAmountService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
