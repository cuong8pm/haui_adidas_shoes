import { IProductAmount } from 'app/shared/model/product-amount.model';

export interface IProductImportHistory {
  id?: number;
  log?: string;
  productAmount?: IProductAmount;
}

export class ProductImportHistory implements IProductImportHistory {
  constructor(public id?: number, public log?: string, public productAmount?: IProductAmount) {}
}
