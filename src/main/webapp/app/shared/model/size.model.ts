import { IProductAmount } from 'app/shared/model/product-amount.model';

export interface ISize {
  id?: number;
  name?: number;
  decription?: string;
  productAmounts?: IProductAmount[];
}

export class Size implements ISize {
  constructor(public id?: number, public name?: number, public decription?: string, public productAmounts?: IProductAmount[]) {}
}
