import { IProduct } from 'app/shared/model/product.model';

export interface IProductImage {
  id?: number;
  photoContentType?: string;
  photo?: any;
  decription?: string;
  product?: IProduct;
}

export class ProductImage implements IProductImage {
  constructor(
    public id?: number,
    public photoContentType?: string,
    public photo?: any,
    public decription?: string,
    public product?: IProduct
  ) {}
}
