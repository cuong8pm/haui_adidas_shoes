import { ICategori } from 'app/shared/model/categori.model';
import { IProductDetails } from 'app/shared/model/product-details.model';
import { IProductImage } from 'app/shared/model/product-image.model';
import { IProductAmount } from 'app/shared/model/product-amount.model';

export interface IProduct {
  id?: number;
  name?: string;
  photoContentType?: string;
  photo?: any;
  price?: number;
  title?: string;
  description?: string;
  discount?: number;
  categori?: ICategori;
  productDetails?: IProductDetails[];
  productImages?: IProductImage[];
  productAmounts?: IProductAmount[];
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public photoContentType?: string,
    public photo?: any,
    public price?: number,
    public title?: string,
    public description?: string,
    public discount?: number,
    public categori?: ICategori,
    public productDetails?: IProductDetails[],
    public productImages?: IProductImage[],
    public productAmounts?: IProductAmount[]
  ) {}
}
