import { IProduct } from 'app/shared/model/product.model';
import { ISize } from 'app/shared/model/size.model';
import { IProductImportHistory } from 'app/shared/model/product-import-history.model';

export interface IProductAmount {
  id?: number;
  total?: number;
  inventory?: number;
  productSold?: number;
  product?: IProduct;
  size?: ISize;
  productImportHistories?: IProductImportHistory[];
}

export class ProductAmount implements IProductAmount {
  constructor(
    public id?: number,
    public total?: number,
    public inventory?: number,
    public productSold?: number,
    public product?: IProduct,
    public size?: ISize,
    public productImportHistories?: IProductImportHistory[]
  ) {}
}
