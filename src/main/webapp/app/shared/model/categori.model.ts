import { IProduct } from 'app/shared/model/product.model';

export interface ICategori {
  id?: number;
  name?: string;
  description?: string;
  products?: IProduct[];
}

export class Categori implements ICategori {
  constructor(public id?: number, public name?: string, public description?: string, public products?: IProduct[]) {}
}
