import { IProduct } from 'app/shared/model/product.model';

export interface IProductDetails {
  id?: number;
  decription?: string;
  product?: IProduct;
}

export class ProductDetails implements IProductDetails {
  constructor(public id?: number, public decription?: string, public product?: IProduct) {}
}
