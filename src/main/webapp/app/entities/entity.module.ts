import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'categori',
        loadChildren: () => import('./categori/categori.module').then(m => m.AdidasShoesCategoriModule),
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.AdidasShoesProductModule),
      },
      {
        path: 'product-details',
        loadChildren: () => import('./product-details/product-details.module').then(m => m.AdidasShoesProductDetailsModule),
      },
      {
        path: 'product-image',
        loadChildren: () => import('./product-image/product-image.module').then(m => m.AdidasShoesProductImageModule),
      },
      {
        path: 'size',
        loadChildren: () => import('./size/size.module').then(m => m.AdidasShoesSizeModule),
      },
      {
        path: 'product-amount',
        loadChildren: () => import('./product-amount/product-amount.module').then(m => m.AdidasShoesProductAmountModule),
      },
      {
        path: 'product-import-history',
        loadChildren: () =>
          import('./product-import-history/product-import-history.module').then(m => m.AdidasShoesProductImportHistoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class AdidasShoesEntityModule {}
