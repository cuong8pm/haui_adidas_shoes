import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISize, Size } from 'app/shared/model/size.model';
import { SizeService } from './size.service';

@Component({
  selector: 'jhi-size-update',
  templateUrl: './size-update.component.html',
})
export class SizeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    decription: [],
  });

  constructor(protected sizeService: SizeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ size }) => {
      this.updateForm(size);
    });
  }

  updateForm(size: ISize): void {
    this.editForm.patchValue({
      id: size.id,
      name: size.name,
      decription: size.decription,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const size = this.createFromForm();
    if (size.id !== undefined) {
      this.subscribeToSaveResponse(this.sizeService.update(size));
    } else {
      this.subscribeToSaveResponse(this.sizeService.create(size));
    }
  }

  private createFromForm(): ISize {
    return {
      ...new Size(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      decription: this.editForm.get(['decription'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISize>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
