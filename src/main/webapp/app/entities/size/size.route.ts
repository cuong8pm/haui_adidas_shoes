import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISize, Size } from 'app/shared/model/size.model';
import { SizeService } from './size.service';
import { SizeComponent } from './size.component';
import { SizeDetailComponent } from './size-detail.component';
import { SizeUpdateComponent } from './size-update.component';

@Injectable({ providedIn: 'root' })
export class SizeResolve implements Resolve<ISize> {
  constructor(private service: SizeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISize> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((size: HttpResponse<Size>) => {
          if (size.body) {
            return of(size.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Size());
  }
}

export const sizeRoute: Routes = [
  {
    path: '',
    component: SizeComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Sizes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SizeDetailComponent,
    resolve: {
      size: SizeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Sizes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SizeUpdateComponent,
    resolve: {
      size: SizeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Sizes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SizeUpdateComponent,
    resolve: {
      size: SizeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Sizes',
    },
    canActivate: [UserRouteAccessService],
  },
];
