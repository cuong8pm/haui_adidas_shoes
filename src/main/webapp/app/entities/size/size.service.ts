import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISize } from 'app/shared/model/size.model';

type EntityResponseType = HttpResponse<ISize>;
type EntityArrayResponseType = HttpResponse<ISize[]>;

@Injectable({ providedIn: 'root' })
export class SizeService {
  public resourceUrl = SERVER_API_URL + 'api/sizes';

  constructor(protected http: HttpClient) {}

  create(size: ISize): Observable<EntityResponseType> {
    return this.http.post<ISize>(this.resourceUrl, size, { observe: 'response' });
  }

  update(size: ISize): Observable<EntityResponseType> {
    return this.http.put<ISize>(this.resourceUrl, size, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISize>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISize[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
