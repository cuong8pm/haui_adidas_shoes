import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISize } from 'app/shared/model/size.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SizeService } from './size.service';
import { SizeDeleteDialogComponent } from './size-delete-dialog.component';

@Component({
  selector: 'jhi-size',
  templateUrl: './size.component.html',
})
export class SizeComponent implements OnInit, OnDestroy {
  sizes: ISize[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected sizeService: SizeService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.sizes = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.sizeService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ISize[]>) => this.paginateSizes(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.sizes = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSizes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISize): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSizes(): void {
    this.eventSubscriber = this.eventManager.subscribe('sizeListModification', () => this.reset());
  }

  delete(size: ISize): void {
    const modalRef = this.modalService.open(SizeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.size = size;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateSizes(data: ISize[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.sizes.push(data[i]);
      }
    }
  }
}
