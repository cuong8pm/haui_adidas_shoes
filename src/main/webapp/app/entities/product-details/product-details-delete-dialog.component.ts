import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductDetails } from 'app/shared/model/product-details.model';
import { ProductDetailsService } from './product-details.service';

@Component({
  templateUrl: './product-details-delete-dialog.component.html',
})
export class ProductDetailsDeleteDialogComponent {
  productDetails?: IProductDetails;

  constructor(
    protected productDetailsService: ProductDetailsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.productDetailsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productDetailsListModification');
      this.activeModal.close();
    });
  }
}
