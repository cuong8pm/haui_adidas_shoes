import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductDetails, ProductDetails } from 'app/shared/model/product-details.model';
import { ProductDetailsService } from './product-details.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';

@Component({
  selector: 'jhi-product-details-update',
  templateUrl: './product-details-update.component.html',
})
export class ProductDetailsUpdateComponent implements OnInit {
  isSaving = false;
  products: IProduct[] = [];

  editForm = this.fb.group({
    id: [],
    decription: [],
    product: [],
  });

  constructor(
    protected productDetailsService: ProductDetailsService,
    protected productService: ProductService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productDetails }) => {
      this.updateForm(productDetails);

      this.productService.query().subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body || []));
    });
  }

  updateForm(productDetails: IProductDetails): void {
    this.editForm.patchValue({
      id: productDetails.id,
      decription: productDetails.decription,
      product: productDetails.product,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productDetails = this.createFromForm();
    if (productDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.productDetailsService.update(productDetails));
    } else {
      this.subscribeToSaveResponse(this.productDetailsService.create(productDetails));
    }
  }

  private createFromForm(): IProductDetails {
    return {
      ...new ProductDetails(),
      id: this.editForm.get(['id'])!.value,
      decription: this.editForm.get(['decription'])!.value,
      product: this.editForm.get(['product'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductDetails>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProduct): any {
    return item.id;
  }
}
