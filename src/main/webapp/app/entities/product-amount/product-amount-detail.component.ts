import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductAmount } from 'app/shared/model/product-amount.model';

@Component({
  selector: 'jhi-product-amount-detail',
  templateUrl: './product-amount-detail.component.html',
})
export class ProductAmountDetailComponent implements OnInit {
  productAmount: IProductAmount | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productAmount }) => (this.productAmount = productAmount));
  }

  previousState(): void {
    window.history.back();
  }
}
