import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductAmount } from 'app/shared/model/product-amount.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ProductAmountService } from './product-amount.service';
import { ProductAmountDeleteDialogComponent } from './product-amount-delete-dialog.component';

@Component({
  selector: 'jhi-product-amount',
  templateUrl: './product-amount.component.html',
})
export class ProductAmountComponent implements OnInit, OnDestroy {
  productAmounts: IProductAmount[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected productAmountService: ProductAmountService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.productAmounts = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.productAmountService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IProductAmount[]>) => this.paginateProductAmounts(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.productAmounts = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductAmounts();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductAmount): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductAmounts(): void {
    this.eventSubscriber = this.eventManager.subscribe('productAmountListModification', () => this.reset());
  }

  delete(productAmount: IProductAmount): void {
    const modalRef = this.modalService.open(ProductAmountDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productAmount = productAmount;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateProductAmounts(data: IProductAmount[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.productAmounts.push(data[i]);
      }
    }
  }
}
