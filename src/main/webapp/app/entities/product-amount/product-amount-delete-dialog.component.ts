import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductAmount } from 'app/shared/model/product-amount.model';
import { ProductAmountService } from './product-amount.service';

@Component({
  templateUrl: './product-amount-delete-dialog.component.html',
})
export class ProductAmountDeleteDialogComponent {
  productAmount?: IProductAmount;

  constructor(
    protected productAmountService: ProductAmountService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.productAmountService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productAmountListModification');
      this.activeModal.close();
    });
  }
}
