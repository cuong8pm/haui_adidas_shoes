import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AdidasShoesSharedModule } from 'app/shared/shared.module';
import { ProductAmountComponent } from './product-amount.component';
import { ProductAmountDetailComponent } from './product-amount-detail.component';
import { ProductAmountUpdateComponent } from './product-amount-update.component';
import { ProductAmountDeleteDialogComponent } from './product-amount-delete-dialog.component';
import { productAmountRoute } from './product-amount.route';

@NgModule({
  imports: [AdidasShoesSharedModule, RouterModule.forChild(productAmountRoute)],
  declarations: [ProductAmountComponent, ProductAmountDetailComponent, ProductAmountUpdateComponent, ProductAmountDeleteDialogComponent],
  entryComponents: [ProductAmountDeleteDialogComponent],
})
export class AdidasShoesProductAmountModule {}
