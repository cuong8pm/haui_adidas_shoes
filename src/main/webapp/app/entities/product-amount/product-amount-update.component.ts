import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductAmount, ProductAmount } from 'app/shared/model/product-amount.model';
import { ProductAmountService } from './product-amount.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { ISize } from 'app/shared/model/size.model';
import { SizeService } from 'app/entities/size/size.service';

type SelectableEntity = IProduct | ISize;

@Component({
  selector: 'jhi-product-amount-update',
  templateUrl: './product-amount-update.component.html',
})
export class ProductAmountUpdateComponent implements OnInit {
  isSaving = false;
  products: IProduct[] = [];
  sizes: ISize[] = [];

  editForm = this.fb.group({
    id: [],
    total: [],
    inventory: [],
    productSold: [],
    product: [],
    size: [],
  });

  constructor(
    protected productAmountService: ProductAmountService,
    protected productService: ProductService,
    protected sizeService: SizeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productAmount }) => {
      this.updateForm(productAmount);

      this.productService.query().subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body || []));

      this.sizeService.query().subscribe((res: HttpResponse<ISize[]>) => (this.sizes = res.body || []));
    });
  }

  updateForm(productAmount: IProductAmount): void {
    this.editForm.patchValue({
      id: productAmount.id,
      total: productAmount.total,
      inventory: productAmount.inventory,
      productSold: productAmount.productSold,
      product: productAmount.product,
      size: productAmount.size,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productAmount = this.createFromForm();
    if (productAmount.id !== undefined) {
      this.subscribeToSaveResponse(this.productAmountService.update(productAmount));
    } else {
      this.subscribeToSaveResponse(this.productAmountService.create(productAmount));
    }
  }

  private createFromForm(): IProductAmount {
    return {
      ...new ProductAmount(),
      id: this.editForm.get(['id'])!.value,
      total: this.editForm.get(['total'])!.value,
      inventory: this.editForm.get(['inventory'])!.value,
      productSold: this.editForm.get(['productSold'])!.value,
      product: this.editForm.get(['product'])!.value,
      size: this.editForm.get(['size'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductAmount>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
