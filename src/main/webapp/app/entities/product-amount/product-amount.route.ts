import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProductAmount, ProductAmount } from 'app/shared/model/product-amount.model';
import { ProductAmountService } from './product-amount.service';
import { ProductAmountComponent } from './product-amount.component';
import { ProductAmountDetailComponent } from './product-amount-detail.component';
import { ProductAmountUpdateComponent } from './product-amount-update.component';

@Injectable({ providedIn: 'root' })
export class ProductAmountResolve implements Resolve<IProductAmount> {
  constructor(private service: ProductAmountService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductAmount> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((productAmount: HttpResponse<ProductAmount>) => {
          if (productAmount.body) {
            return of(productAmount.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProductAmount());
  }
}

export const productAmountRoute: Routes = [
  {
    path: '',
    component: ProductAmountComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductAmounts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductAmountDetailComponent,
    resolve: {
      productAmount: ProductAmountResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductAmounts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductAmountUpdateComponent,
    resolve: {
      productAmount: ProductAmountResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductAmounts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductAmountUpdateComponent,
    resolve: {
      productAmount: ProductAmountResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductAmounts',
    },
    canActivate: [UserRouteAccessService],
  },
];
