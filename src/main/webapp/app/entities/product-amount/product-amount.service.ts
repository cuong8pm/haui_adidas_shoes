import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductAmount } from 'app/shared/model/product-amount.model';

type EntityResponseType = HttpResponse<IProductAmount>;
type EntityArrayResponseType = HttpResponse<IProductAmount[]>;

@Injectable({ providedIn: 'root' })
export class ProductAmountService {
  public resourceUrl = SERVER_API_URL + 'api/product-amounts';

  constructor(protected http: HttpClient) {}

  create(productAmount: IProductAmount): Observable<EntityResponseType> {
    return this.http.post<IProductAmount>(this.resourceUrl, productAmount, { observe: 'response' });
  }

  update(productAmount: IProductAmount): Observable<EntityResponseType> {
    return this.http.put<IProductAmount>(this.resourceUrl, productAmount, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductAmount>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductAmount[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
