import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductImportHistory } from 'app/shared/model/product-import-history.model';

@Component({
  selector: 'jhi-product-import-history-detail',
  templateUrl: './product-import-history-detail.component.html',
})
export class ProductImportHistoryDetailComponent implements OnInit {
  productImportHistory: IProductImportHistory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productImportHistory }) => (this.productImportHistory = productImportHistory));
  }

  previousState(): void {
    window.history.back();
  }
}
