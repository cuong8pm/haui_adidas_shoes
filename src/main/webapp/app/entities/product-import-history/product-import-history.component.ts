import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductImportHistory } from 'app/shared/model/product-import-history.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ProductImportHistoryService } from './product-import-history.service';
import { ProductImportHistoryDeleteDialogComponent } from './product-import-history-delete-dialog.component';

@Component({
  selector: 'jhi-product-import-history',
  templateUrl: './product-import-history.component.html',
})
export class ProductImportHistoryComponent implements OnInit, OnDestroy {
  productImportHistories: IProductImportHistory[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected productImportHistoryService: ProductImportHistoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.productImportHistories = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.productImportHistoryService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IProductImportHistory[]>) => this.paginateProductImportHistories(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.productImportHistories = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductImportHistories();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductImportHistory): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductImportHistories(): void {
    this.eventSubscriber = this.eventManager.subscribe('productImportHistoryListModification', () => this.reset());
  }

  delete(productImportHistory: IProductImportHistory): void {
    const modalRef = this.modalService.open(ProductImportHistoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productImportHistory = productImportHistory;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateProductImportHistories(data: IProductImportHistory[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.productImportHistories.push(data[i]);
      }
    }
  }
}
