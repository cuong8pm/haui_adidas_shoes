import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AdidasShoesSharedModule } from 'app/shared/shared.module';
import { ProductImportHistoryComponent } from './product-import-history.component';
import { ProductImportHistoryDetailComponent } from './product-import-history-detail.component';
import { ProductImportHistoryUpdateComponent } from './product-import-history-update.component';
import { ProductImportHistoryDeleteDialogComponent } from './product-import-history-delete-dialog.component';
import { productImportHistoryRoute } from './product-import-history.route';

@NgModule({
  imports: [AdidasShoesSharedModule, RouterModule.forChild(productImportHistoryRoute)],
  declarations: [
    ProductImportHistoryComponent,
    ProductImportHistoryDetailComponent,
    ProductImportHistoryUpdateComponent,
    ProductImportHistoryDeleteDialogComponent,
  ],
  entryComponents: [ProductImportHistoryDeleteDialogComponent],
})
export class AdidasShoesProductImportHistoryModule {}
