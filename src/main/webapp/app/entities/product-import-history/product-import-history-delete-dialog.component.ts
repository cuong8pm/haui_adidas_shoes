import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductImportHistory } from 'app/shared/model/product-import-history.model';
import { ProductImportHistoryService } from './product-import-history.service';

@Component({
  templateUrl: './product-import-history-delete-dialog.component.html',
})
export class ProductImportHistoryDeleteDialogComponent {
  productImportHistory?: IProductImportHistory;

  constructor(
    protected productImportHistoryService: ProductImportHistoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.productImportHistoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productImportHistoryListModification');
      this.activeModal.close();
    });
  }
}
