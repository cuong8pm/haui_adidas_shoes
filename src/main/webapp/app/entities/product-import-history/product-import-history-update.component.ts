import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductImportHistory, ProductImportHistory } from 'app/shared/model/product-import-history.model';
import { ProductImportHistoryService } from './product-import-history.service';
import { IProductAmount } from 'app/shared/model/product-amount.model';
import { ProductAmountService } from 'app/entities/product-amount/product-amount.service';

@Component({
  selector: 'jhi-product-import-history-update',
  templateUrl: './product-import-history-update.component.html',
})
export class ProductImportHistoryUpdateComponent implements OnInit {
  isSaving = false;
  productamounts: IProductAmount[] = [];

  editForm = this.fb.group({
    id: [],
    log: [],
    productAmount: [],
  });

  constructor(
    protected productImportHistoryService: ProductImportHistoryService,
    protected productAmountService: ProductAmountService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productImportHistory }) => {
      this.updateForm(productImportHistory);

      this.productAmountService.query().subscribe((res: HttpResponse<IProductAmount[]>) => (this.productamounts = res.body || []));
    });
  }

  updateForm(productImportHistory: IProductImportHistory): void {
    this.editForm.patchValue({
      id: productImportHistory.id,
      log: productImportHistory.log,
      productAmount: productImportHistory.productAmount,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productImportHistory = this.createFromForm();
    if (productImportHistory.id !== undefined) {
      this.subscribeToSaveResponse(this.productImportHistoryService.update(productImportHistory));
    } else {
      this.subscribeToSaveResponse(this.productImportHistoryService.create(productImportHistory));
    }
  }

  private createFromForm(): IProductImportHistory {
    return {
      ...new ProductImportHistory(),
      id: this.editForm.get(['id'])!.value,
      log: this.editForm.get(['log'])!.value,
      productAmount: this.editForm.get(['productAmount'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductImportHistory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProductAmount): any {
    return item.id;
  }
}
