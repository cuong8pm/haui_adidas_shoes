import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductImage } from 'app/shared/model/product-image.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ProductImageService } from './product-image.service';
import { ProductImageDeleteDialogComponent } from './product-image-delete-dialog.component';

@Component({
  selector: 'jhi-product-image',
  templateUrl: './product-image.component.html',
})
export class ProductImageComponent implements OnInit, OnDestroy {
  productImages: IProductImage[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected productImageService: ProductImageService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.productImages = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.productImageService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IProductImage[]>) => this.paginateProductImages(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.productImages = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductImages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductImage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInProductImages(): void {
    this.eventSubscriber = this.eventManager.subscribe('productImageListModification', () => this.reset());
  }

  delete(productImage: IProductImage): void {
    const modalRef = this.modalService.open(ProductImageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productImage = productImage;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateProductImages(data: IProductImage[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.productImages.push(data[i]);
      }
    }
  }
}
