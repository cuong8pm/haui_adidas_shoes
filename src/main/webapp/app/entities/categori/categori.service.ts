import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICategori } from 'app/shared/model/categori.model';

type EntityResponseType = HttpResponse<ICategori>;
type EntityArrayResponseType = HttpResponse<ICategori[]>;

@Injectable({ providedIn: 'root' })
export class CategoriService {
  public resourceUrl = SERVER_API_URL + 'api/categoris';

  constructor(protected http: HttpClient) {}

  create(categori: ICategori): Observable<EntityResponseType> {
    return this.http.post<ICategori>(this.resourceUrl, categori, { observe: 'response' });
  }

  update(categori: ICategori): Observable<EntityResponseType> {
    return this.http.put<ICategori>(this.resourceUrl, categori, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICategori>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategori[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
