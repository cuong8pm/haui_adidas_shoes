import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICategori, Categori } from 'app/shared/model/categori.model';
import { CategoriService } from './categori.service';
import { CategoriComponent } from './categori.component';
import { CategoriDetailComponent } from './categori-detail.component';
import { CategoriUpdateComponent } from './categori-update.component';

@Injectable({ providedIn: 'root' })
export class CategoriResolve implements Resolve<ICategori> {
  constructor(private service: CategoriService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICategori> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((categori: HttpResponse<Categori>) => {
          if (categori.body) {
            return of(categori.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Categori());
  }
}

export const categoriRoute: Routes = [
  {
    path: '',
    component: CategoriComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Categoris',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CategoriDetailComponent,
    resolve: {
      categori: CategoriResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Categoris',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CategoriUpdateComponent,
    resolve: {
      categori: CategoriResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Categoris',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CategoriUpdateComponent,
    resolve: {
      categori: CategoriResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Categoris',
    },
    canActivate: [UserRouteAccessService],
  },
];
