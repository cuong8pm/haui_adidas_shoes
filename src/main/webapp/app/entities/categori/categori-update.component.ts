import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICategori, Categori } from 'app/shared/model/categori.model';
import { CategoriService } from './categori.service';

@Component({
  selector: 'jhi-categori-update',
  templateUrl: './categori-update.component.html',
})
export class CategoriUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    description: [],
  });

  constructor(protected categoriService: CategoriService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categori }) => {
      this.updateForm(categori);
    });
  }

  updateForm(categori: ICategori): void {
    this.editForm.patchValue({
      id: categori.id,
      name: categori.name,
      description: categori.description,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const categori = this.createFromForm();
    if (categori.id !== undefined) {
      this.subscribeToSaveResponse(this.categoriService.update(categori));
    } else {
      this.subscribeToSaveResponse(this.categoriService.create(categori));
    }
  }

  private createFromForm(): ICategori {
    return {
      ...new Categori(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICategori>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
