import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AdidasShoesSharedModule } from 'app/shared/shared.module';
import { CategoriComponent } from './categori.component';
import { CategoriDetailComponent } from './categori-detail.component';
import { CategoriUpdateComponent } from './categori-update.component';
import { CategoriDeleteDialogComponent } from './categori-delete-dialog.component';
import { categoriRoute } from './categori.route';

@NgModule({
  imports: [AdidasShoesSharedModule, RouterModule.forChild(categoriRoute)],
  declarations: [CategoriComponent, CategoriDetailComponent, CategoriUpdateComponent, CategoriDeleteDialogComponent],
  entryComponents: [CategoriDeleteDialogComponent],
})
export class AdidasShoesCategoriModule {}
