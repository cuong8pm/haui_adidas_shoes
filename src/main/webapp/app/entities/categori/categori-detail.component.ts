import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICategori } from 'app/shared/model/categori.model';

@Component({
  selector: 'jhi-categori-detail',
  templateUrl: './categori-detail.component.html',
})
export class CategoriDetailComponent implements OnInit {
  categori: ICategori | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categori }) => (this.categori = categori));
  }

  previousState(): void {
    window.history.back();
  }
}
