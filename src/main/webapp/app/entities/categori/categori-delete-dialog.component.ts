import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICategori } from 'app/shared/model/categori.model';
import { CategoriService } from './categori.service';

@Component({
  templateUrl: './categori-delete-dialog.component.html',
})
export class CategoriDeleteDialogComponent {
  categori?: ICategori;

  constructor(protected categoriService: CategoriService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.categoriService.delete(id).subscribe(() => {
      this.eventManager.broadcast('categoriListModification');
      this.activeModal.close();
    });
  }
}
