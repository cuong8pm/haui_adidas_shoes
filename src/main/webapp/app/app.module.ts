import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AdidasShoesSharedModule } from 'app/shared/shared.module';
import { AdidasShoesCoreModule } from 'app/core/core.module';
import { AdidasShoesAppRoutingModule } from './app-routing.module';
import { AdidasShoesHomeModule } from './home/home.module';
import { AdidasShoesEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    AdidasShoesSharedModule,
    AdidasShoesCoreModule,
    AdidasShoesHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AdidasShoesEntityModule,
    AdidasShoesAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class AdidasShoesAppModule {}
