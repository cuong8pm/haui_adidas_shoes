import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AdidasShoesTestModule } from '../../../test.module';
import { CategoriUpdateComponent } from 'app/entities/categori/categori-update.component';
import { CategoriService } from 'app/entities/categori/categori.service';
import { Categori } from 'app/shared/model/categori.model';

describe('Component Tests', () => {
  describe('Categori Management Update Component', () => {
    let comp: CategoriUpdateComponent;
    let fixture: ComponentFixture<CategoriUpdateComponent>;
    let service: CategoriService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AdidasShoesTestModule],
        declarations: [CategoriUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CategoriUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CategoriUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CategoriService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Categori(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Categori();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
