import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AdidasShoesTestModule } from '../../../test.module';
import { CategoriDetailComponent } from 'app/entities/categori/categori-detail.component';
import { Categori } from 'app/shared/model/categori.model';

describe('Component Tests', () => {
  describe('Categori Management Detail Component', () => {
    let comp: CategoriDetailComponent;
    let fixture: ComponentFixture<CategoriDetailComponent>;
    const route = ({ data: of({ categori: new Categori(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AdidasShoesTestModule],
        declarations: [CategoriDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CategoriDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CategoriDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load categori on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.categori).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
