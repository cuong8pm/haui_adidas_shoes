import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AdidasShoesTestModule } from '../../../test.module';
import { ProductAmountUpdateComponent } from 'app/entities/product-amount/product-amount-update.component';
import { ProductAmountService } from 'app/entities/product-amount/product-amount.service';
import { ProductAmount } from 'app/shared/model/product-amount.model';

describe('Component Tests', () => {
  describe('ProductAmount Management Update Component', () => {
    let comp: ProductAmountUpdateComponent;
    let fixture: ComponentFixture<ProductAmountUpdateComponent>;
    let service: ProductAmountService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AdidasShoesTestModule],
        declarations: [ProductAmountUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProductAmountUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductAmountUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductAmountService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductAmount(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductAmount();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
