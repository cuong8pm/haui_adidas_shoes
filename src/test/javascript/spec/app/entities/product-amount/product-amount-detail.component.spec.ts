import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AdidasShoesTestModule } from '../../../test.module';
import { ProductAmountDetailComponent } from 'app/entities/product-amount/product-amount-detail.component';
import { ProductAmount } from 'app/shared/model/product-amount.model';

describe('Component Tests', () => {
  describe('ProductAmount Management Detail Component', () => {
    let comp: ProductAmountDetailComponent;
    let fixture: ComponentFixture<ProductAmountDetailComponent>;
    const route = ({ data: of({ productAmount: new ProductAmount(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AdidasShoesTestModule],
        declarations: [ProductAmountDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProductAmountDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductAmountDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load productAmount on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.productAmount).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
