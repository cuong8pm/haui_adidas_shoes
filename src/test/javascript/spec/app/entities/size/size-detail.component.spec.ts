import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AdidasShoesTestModule } from '../../../test.module';
import { SizeDetailComponent } from 'app/entities/size/size-detail.component';
import { Size } from 'app/shared/model/size.model';

describe('Component Tests', () => {
  describe('Size Management Detail Component', () => {
    let comp: SizeDetailComponent;
    let fixture: ComponentFixture<SizeDetailComponent>;
    const route = ({ data: of({ size: new Size(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AdidasShoesTestModule],
        declarations: [SizeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(SizeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SizeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load size on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.size).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
