import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AdidasShoesTestModule } from '../../../test.module';
import { ProductImportHistoryUpdateComponent } from 'app/entities/product-import-history/product-import-history-update.component';
import { ProductImportHistoryService } from 'app/entities/product-import-history/product-import-history.service';
import { ProductImportHistory } from 'app/shared/model/product-import-history.model';

describe('Component Tests', () => {
  describe('ProductImportHistory Management Update Component', () => {
    let comp: ProductImportHistoryUpdateComponent;
    let fixture: ComponentFixture<ProductImportHistoryUpdateComponent>;
    let service: ProductImportHistoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AdidasShoesTestModule],
        declarations: [ProductImportHistoryUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProductImportHistoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductImportHistoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductImportHistoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductImportHistory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductImportHistory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
