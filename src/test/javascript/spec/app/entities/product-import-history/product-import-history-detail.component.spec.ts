import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AdidasShoesTestModule } from '../../../test.module';
import { ProductImportHistoryDetailComponent } from 'app/entities/product-import-history/product-import-history-detail.component';
import { ProductImportHistory } from 'app/shared/model/product-import-history.model';

describe('Component Tests', () => {
  describe('ProductImportHistory Management Detail Component', () => {
    let comp: ProductImportHistoryDetailComponent;
    let fixture: ComponentFixture<ProductImportHistoryDetailComponent>;
    const route = ({ data: of({ productImportHistory: new ProductImportHistory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AdidasShoesTestModule],
        declarations: [ProductImportHistoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProductImportHistoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductImportHistoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load productImportHistory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.productImportHistory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
