package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.AdidasShoesApp;
import com.haui.adidasshoes.domain.Categori;
import com.haui.adidasshoes.repository.CategoriRepository;
import com.haui.adidasshoes.service.CategoriService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CategoriResource} REST controller.
 */
@SpringBootTest(classes = AdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CategoriResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CategoriRepository categoriRepository;

    @Autowired
    private CategoriService categoriService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCategoriMockMvc;

    private Categori categori;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Categori createEntity(EntityManager em) {
        Categori categori = new Categori()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return categori;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Categori createUpdatedEntity(EntityManager em) {
        Categori categori = new Categori()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        return categori;
    }

    @BeforeEach
    public void initTest() {
        categori = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategori() throws Exception {
        int databaseSizeBeforeCreate = categoriRepository.findAll().size();
        // Create the Categori
        restCategoriMockMvc.perform(post("/api/categoris")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categori)))
            .andExpect(status().isCreated());

        // Validate the Categori in the database
        List<Categori> categoriList = categoriRepository.findAll();
        assertThat(categoriList).hasSize(databaseSizeBeforeCreate + 1);
        Categori testCategori = categoriList.get(categoriList.size() - 1);
        assertThat(testCategori.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCategori.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCategoriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categoriRepository.findAll().size();

        // Create the Categori with an existing ID
        categori.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategoriMockMvc.perform(post("/api/categoris")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categori)))
            .andExpect(status().isBadRequest());

        // Validate the Categori in the database
        List<Categori> categoriList = categoriRepository.findAll();
        assertThat(categoriList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCategoris() throws Exception {
        // Initialize the database
        categoriRepository.saveAndFlush(categori);

        // Get all the categoriList
        restCategoriMockMvc.perform(get("/api/categoris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categori.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getCategori() throws Exception {
        // Initialize the database
        categoriRepository.saveAndFlush(categori);

        // Get the categori
        restCategoriMockMvc.perform(get("/api/categoris/{id}", categori.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(categori.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingCategori() throws Exception {
        // Get the categori
        restCategoriMockMvc.perform(get("/api/categoris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategori() throws Exception {
        // Initialize the database
        categoriService.save(categori);

        int databaseSizeBeforeUpdate = categoriRepository.findAll().size();

        // Update the categori
        Categori updatedCategori = categoriRepository.findById(categori.getId()).get();
        // Disconnect from session so that the updates on updatedCategori are not directly saved in db
        em.detach(updatedCategori);
        updatedCategori
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restCategoriMockMvc.perform(put("/api/categoris")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCategori)))
            .andExpect(status().isOk());

        // Validate the Categori in the database
        List<Categori> categoriList = categoriRepository.findAll();
        assertThat(categoriList).hasSize(databaseSizeBeforeUpdate);
        Categori testCategori = categoriList.get(categoriList.size() - 1);
        assertThat(testCategori.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCategori.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCategori() throws Exception {
        int databaseSizeBeforeUpdate = categoriRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategoriMockMvc.perform(put("/api/categoris")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categori)))
            .andExpect(status().isBadRequest());

        // Validate the Categori in the database
        List<Categori> categoriList = categoriRepository.findAll();
        assertThat(categoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCategori() throws Exception {
        // Initialize the database
        categoriService.save(categori);

        int databaseSizeBeforeDelete = categoriRepository.findAll().size();

        // Delete the categori
        restCategoriMockMvc.perform(delete("/api/categoris/{id}", categori.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Categori> categoriList = categoriRepository.findAll();
        assertThat(categoriList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
