package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.AdidasShoesApp;
import com.haui.adidasshoes.domain.ProductAmount;
import com.haui.adidasshoes.repository.ProductAmountRepository;
import com.haui.adidasshoes.service.ProductAmountService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductAmountResource} REST controller.
 */
@SpringBootTest(classes = AdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProductAmountResourceIT {

    private static final Double DEFAULT_TOTAL = 1D;
    private static final Double UPDATED_TOTAL = 2D;

    private static final Double DEFAULT_INVENTORY = 1D;
    private static final Double UPDATED_INVENTORY = 2D;

    private static final Double DEFAULT_PRODUCT_SOLD = 1D;
    private static final Double UPDATED_PRODUCT_SOLD = 2D;

    @Autowired
    private ProductAmountRepository productAmountRepository;

    @Autowired
    private ProductAmountService productAmountService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductAmountMockMvc;

    private ProductAmount productAmount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductAmount createEntity(EntityManager em) {
        ProductAmount productAmount = new ProductAmount()
            .total(DEFAULT_TOTAL)
            .inventory(DEFAULT_INVENTORY)
            .productSold(DEFAULT_PRODUCT_SOLD);
        return productAmount;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductAmount createUpdatedEntity(EntityManager em) {
        ProductAmount productAmount = new ProductAmount()
            .total(UPDATED_TOTAL)
            .inventory(UPDATED_INVENTORY)
            .productSold(UPDATED_PRODUCT_SOLD);
        return productAmount;
    }

    @BeforeEach
    public void initTest() {
        productAmount = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductAmount() throws Exception {
        int databaseSizeBeforeCreate = productAmountRepository.findAll().size();
        // Create the ProductAmount
        restProductAmountMockMvc.perform(post("/api/product-amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productAmount)))
            .andExpect(status().isCreated());

        // Validate the ProductAmount in the database
        List<ProductAmount> productAmountList = productAmountRepository.findAll();
        assertThat(productAmountList).hasSize(databaseSizeBeforeCreate + 1);
        ProductAmount testProductAmount = productAmountList.get(productAmountList.size() - 1);
        assertThat(testProductAmount.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testProductAmount.getInventory()).isEqualTo(DEFAULT_INVENTORY);
        assertThat(testProductAmount.getProductSold()).isEqualTo(DEFAULT_PRODUCT_SOLD);
    }

    @Test
    @Transactional
    public void createProductAmountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productAmountRepository.findAll().size();

        // Create the ProductAmount with an existing ID
        productAmount.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductAmountMockMvc.perform(post("/api/product-amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productAmount)))
            .andExpect(status().isBadRequest());

        // Validate the ProductAmount in the database
        List<ProductAmount> productAmountList = productAmountRepository.findAll();
        assertThat(productAmountList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProductAmounts() throws Exception {
        // Initialize the database
        productAmountRepository.saveAndFlush(productAmount);

        // Get all the productAmountList
        restProductAmountMockMvc.perform(get("/api/product-amounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productAmount.getId().intValue())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.doubleValue())))
            .andExpect(jsonPath("$.[*].inventory").value(hasItem(DEFAULT_INVENTORY.doubleValue())))
            .andExpect(jsonPath("$.[*].productSold").value(hasItem(DEFAULT_PRODUCT_SOLD.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getProductAmount() throws Exception {
        // Initialize the database
        productAmountRepository.saveAndFlush(productAmount);

        // Get the productAmount
        restProductAmountMockMvc.perform(get("/api/product-amounts/{id}", productAmount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productAmount.getId().intValue()))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.doubleValue()))
            .andExpect(jsonPath("$.inventory").value(DEFAULT_INVENTORY.doubleValue()))
            .andExpect(jsonPath("$.productSold").value(DEFAULT_PRODUCT_SOLD.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProductAmount() throws Exception {
        // Get the productAmount
        restProductAmountMockMvc.perform(get("/api/product-amounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductAmount() throws Exception {
        // Initialize the database
        productAmountService.save(productAmount);

        int databaseSizeBeforeUpdate = productAmountRepository.findAll().size();

        // Update the productAmount
        ProductAmount updatedProductAmount = productAmountRepository.findById(productAmount.getId()).get();
        // Disconnect from session so that the updates on updatedProductAmount are not directly saved in db
        em.detach(updatedProductAmount);
        updatedProductAmount
            .total(UPDATED_TOTAL)
            .inventory(UPDATED_INVENTORY)
            .productSold(UPDATED_PRODUCT_SOLD);

        restProductAmountMockMvc.perform(put("/api/product-amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductAmount)))
            .andExpect(status().isOk());

        // Validate the ProductAmount in the database
        List<ProductAmount> productAmountList = productAmountRepository.findAll();
        assertThat(productAmountList).hasSize(databaseSizeBeforeUpdate);
        ProductAmount testProductAmount = productAmountList.get(productAmountList.size() - 1);
        assertThat(testProductAmount.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testProductAmount.getInventory()).isEqualTo(UPDATED_INVENTORY);
        assertThat(testProductAmount.getProductSold()).isEqualTo(UPDATED_PRODUCT_SOLD);
    }

    @Test
    @Transactional
    public void updateNonExistingProductAmount() throws Exception {
        int databaseSizeBeforeUpdate = productAmountRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductAmountMockMvc.perform(put("/api/product-amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productAmount)))
            .andExpect(status().isBadRequest());

        // Validate the ProductAmount in the database
        List<ProductAmount> productAmountList = productAmountRepository.findAll();
        assertThat(productAmountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductAmount() throws Exception {
        // Initialize the database
        productAmountService.save(productAmount);

        int databaseSizeBeforeDelete = productAmountRepository.findAll().size();

        // Delete the productAmount
        restProductAmountMockMvc.perform(delete("/api/product-amounts/{id}", productAmount.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductAmount> productAmountList = productAmountRepository.findAll();
        assertThat(productAmountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
