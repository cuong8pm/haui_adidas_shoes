package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.AdidasShoesApp;
import com.haui.adidasshoes.domain.ProductImportHistory;
import com.haui.adidasshoes.repository.ProductImportHistoryRepository;
import com.haui.adidasshoes.service.ProductImportHistoryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductImportHistoryResource} REST controller.
 */
@SpringBootTest(classes = AdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProductImportHistoryResourceIT {

    private static final String DEFAULT_LOG = "AAAAAAAAAA";
    private static final String UPDATED_LOG = "BBBBBBBBBB";

    @Autowired
    private ProductImportHistoryRepository productImportHistoryRepository;

    @Autowired
    private ProductImportHistoryService productImportHistoryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductImportHistoryMockMvc;

    private ProductImportHistory productImportHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductImportHistory createEntity(EntityManager em) {
        ProductImportHistory productImportHistory = new ProductImportHistory()
            .log(DEFAULT_LOG);
        return productImportHistory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductImportHistory createUpdatedEntity(EntityManager em) {
        ProductImportHistory productImportHistory = new ProductImportHistory()
            .log(UPDATED_LOG);
        return productImportHistory;
    }

    @BeforeEach
    public void initTest() {
        productImportHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductImportHistory() throws Exception {
        int databaseSizeBeforeCreate = productImportHistoryRepository.findAll().size();
        // Create the ProductImportHistory
        restProductImportHistoryMockMvc.perform(post("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productImportHistory)))
            .andExpect(status().isCreated());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        ProductImportHistory testProductImportHistory = productImportHistoryList.get(productImportHistoryList.size() - 1);
        assertThat(testProductImportHistory.getLog()).isEqualTo(DEFAULT_LOG);
    }

    @Test
    @Transactional
    public void createProductImportHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productImportHistoryRepository.findAll().size();

        // Create the ProductImportHistory with an existing ID
        productImportHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductImportHistoryMockMvc.perform(post("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productImportHistory)))
            .andExpect(status().isBadRequest());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProductImportHistories() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productImportHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].log").value(hasItem(DEFAULT_LOG)));
    }
    
    @Test
    @Transactional
    public void getProductImportHistory() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get the productImportHistory
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories/{id}", productImportHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productImportHistory.getId().intValue()))
            .andExpect(jsonPath("$.log").value(DEFAULT_LOG));
    }
    @Test
    @Transactional
    public void getNonExistingProductImportHistory() throws Exception {
        // Get the productImportHistory
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductImportHistory() throws Exception {
        // Initialize the database
        productImportHistoryService.save(productImportHistory);

        int databaseSizeBeforeUpdate = productImportHistoryRepository.findAll().size();

        // Update the productImportHistory
        ProductImportHistory updatedProductImportHistory = productImportHistoryRepository.findById(productImportHistory.getId()).get();
        // Disconnect from session so that the updates on updatedProductImportHistory are not directly saved in db
        em.detach(updatedProductImportHistory);
        updatedProductImportHistory
            .log(UPDATED_LOG);

        restProductImportHistoryMockMvc.perform(put("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductImportHistory)))
            .andExpect(status().isOk());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeUpdate);
        ProductImportHistory testProductImportHistory = productImportHistoryList.get(productImportHistoryList.size() - 1);
        assertThat(testProductImportHistory.getLog()).isEqualTo(UPDATED_LOG);
    }

    @Test
    @Transactional
    public void updateNonExistingProductImportHistory() throws Exception {
        int databaseSizeBeforeUpdate = productImportHistoryRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductImportHistoryMockMvc.perform(put("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productImportHistory)))
            .andExpect(status().isBadRequest());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductImportHistory() throws Exception {
        // Initialize the database
        productImportHistoryService.save(productImportHistory);

        int databaseSizeBeforeDelete = productImportHistoryRepository.findAll().size();

        // Delete the productImportHistory
        restProductImportHistoryMockMvc.perform(delete("/api/product-import-histories/{id}", productImportHistory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
