package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class CategoriTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Categori.class);
        Categori categori1 = new Categori();
        categori1.setId(1L);
        Categori categori2 = new Categori();
        categori2.setId(categori1.getId());
        assertThat(categori1).isEqualTo(categori2);
        categori2.setId(2L);
        assertThat(categori1).isNotEqualTo(categori2);
        categori1.setId(null);
        assertThat(categori1).isNotEqualTo(categori2);
    }
}
