package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class ProductImportHistoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductImportHistory.class);
        ProductImportHistory productImportHistory1 = new ProductImportHistory();
        productImportHistory1.setId(1L);
        ProductImportHistory productImportHistory2 = new ProductImportHistory();
        productImportHistory2.setId(productImportHistory1.getId());
        assertThat(productImportHistory1).isEqualTo(productImportHistory2);
        productImportHistory2.setId(2L);
        assertThat(productImportHistory1).isNotEqualTo(productImportHistory2);
        productImportHistory1.setId(null);
        assertThat(productImportHistory1).isNotEqualTo(productImportHistory2);
    }
}
